import java.io.*;

public class Lib {

	String FileName;
	File file = null;
	String[] sortWords = new String[11];
    int[] sortCount = new int[11];
	
//读取文件	
	public BufferedReader Readfile(String FileName)
	{
		file = new File(FileName);
		BufferedReader Reader = null;
		
		try {
			if(file.exists())
				file.createNewFile();
			//字符流形式读取
			Reader = new BufferedReader(new FileReader(file));
		}catch(Exception e){
			
		}
		return Reader;
	}
	
//统计字符数
	public long CountChar(BufferedReader text) {
		int charnum;
		long number = 0L;
		
		try {
			while((charnum=text.read())!=-1) {   //text.read()逐个以数字的形式读取文本的字符
				if(charnum < 128 && charnum >=0)
					number++;
			}
		}catch(Exception e) {}
		
		return number;	
	}
	
//统计单词数
	public long CountWords(BufferedReader text){
		long number = 0L;
		String s = new String();
		int oneword;
		
		try {
			while((oneword=text.read())!=-1) {
				char ch = (char)oneword;
				
				if(ch >= 'a' && ch <= 'z' 
				   ||ch >= 'A' && ch <='Z'
				   || ch >= '0' && ch <= '9')
				     s=s+ch;
				else {
					if(s != null && Yes(s))
						number++;
					String t = null;
					s = t;
				}
			}
            if (Yes(s))
                number++;
                
		}catch(Exception e) {}
		return number;
	}
//上个方法调用的函数，判断符不符合单词的要求
	public boolean Yes(String s) {
		if(s==null)
			return false;
		for(int i=0;i<s.length();i++) {
			if(i < 4 && s.charAt(i) < 'A' 
			      && s.charAt(i) > 'Z' 
                                                       &&  s.charAt(i) < 'a' && s.charAt(i) > 'z' )
				return false;
		} 
		return true;
	}
	
//统计行数
	public int CountLines(BufferedReader text) {
		int rows = 0;
		String str = new String();
		
		try {
			while((str = text.readLine()) != null) {
				//“\\s”是正则表达式，表示所以空白字符（换行、空格等），将这些空白字符换掉
				String temp = str.replaceAll("\\s","");
				if(str.length()!=0)
					rows++;
			}
				  
		}catch(Exception e) {}
		
		return rows;
	}
//统计单词频数
	public void CountWordsNum(BufferedReader text,int n) {
		int oneword;
		String str = new String();
		
		String[] words = new String[n];
		int[] counts = new int[n];
		
		for(int i=0;i<n;i++)
			counts[i] = 0;
		int j = 0;
		
		try {
		   while((oneword = text.read())!=-1) {
			   char ch = (char)oneword;
			   
			   if(ch >= 'a' && ch <= 'z' 
					   ||ch >= 'A' && ch <='Z'
					   || ch >= '0' && ch <= '9') {
				   if(str == null) {
					   String tem = new String();
					   str = tem;
				   }
				   str=str+ch;
			   }else {
				   if(str!="" && Yes(str)) {
					   String temp = str.toLowerCase();
					 //单词不在数组中，则存入计数 ，已在数组中，则频数加1
					   if(!Ifexist(words,temp,counts) && j<words.length) {
						   words[j] = str;
						   counts[j]++;
						   if(j == words.length-1)
							   break;
						   j++;
					   }
					   str = null;   
				   }
			   }  
		   }
		//对单词排序
		  SortWords(words, counts);
		}catch(Exception e) {	}
	}
	//判断数组中是否有相同的单词,若有相同的单词则计数
	public boolean Ifexist(String[] words,String str,int[] counts) {
		for(int i=0;i<words.length && words.length >=1;i++) {
			if (str.equals(words[i])) {
                counts[i]++;
                return true;
            }
		}
		return false;
	}
	//按单词的频数，从大到小排序，取前10个
	public void SortWords(String[] words, int[] counts) {

        for (int i=0; i<counts.length && words[i]!=null; i++) {
            for (int j=i+1 ; j<counts.length && words[j]!=null; j++){

                if (counts[i]<counts[j]){
                    //交换两个单词的频数的位置
                	int temp;
                    temp = counts[j];
                    counts[j] = counts[i];
                    counts[i] = temp;
                	//交换两个单词的位置
                    String tempS = new String ();
                    tempS = words[j];
                    words[j] = words[i];
                    words[i] = tempS;
                }
                else if (counts[i]==counts[j]) {
                    if (words[i].compareTo(words[j]) > 0){
                    	//交换两个单词的频数的位置
                        int temp = counts[j];
                        counts[j] = counts[i];
                        counts[i] = temp;
                    	//交换两个单词的位置
                        String tempS = new String ();
                        tempS = words[j];
                        words[j] = words[i];
                        words[i] = tempS;
                    }
                }
            }

        }
        for (int i=0; i<10 && i<words.length; i++) {
            sortWords[i] = words[i];
            sortCount[i] = counts[i];
        }
    }
	public String[] GetWords() {
        return sortWords;
    }

    public int[] GetCount() {
        return sortCount;
    }
	
} 
